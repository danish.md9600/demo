import React, { useState, useEffect } from 'react'

//get local storage
const getLocalItems = () => {
    let list = localStorage.getItem('lists');
    console.log(list);

    if (list) {
        return JSON.parse(localStorage.getItem('lists'));
    } else {
        return [];
    }
}
function Todo() {
    const [inputData, setInputData] = useState('');
    const [items, setItems] = useState(getLocalItems());
    const [toggleSubmit, setToggleSubmit] = useState(true);
    const [isEditItem, setIsEditItem] = useState(null);

    const addItem = () => {
        if (!inputData) {
            alert("Add Task");
        } else if (inputData && !toggleSubmit) {
            setItems(
                items.map((elem) => {
                    if (elem.id === isEditItem) {
                        return { ...elem, name: inputData }
                    }
                    return elem;
                })
            )

            setToggleSubmit(true);

            setInputData('');

            setIsEditItem(null);

        } else {
            const allInputData = { id: new Date().getTime().toString(), name: inputData }
            setItems([...items, allInputData]);
            setInputData('');
            // console.log(inputData);
        }
    }

    //Edit Item when you user click button
    //1. get the id and name of data which user clicked to edit.
    //2. set the toggle mode change the submit button into edit button.
    //3. now update the value of setInput with the new updated value or edit.
    //4. to pass the current element id to new state variable for refrence.

    const editItem = (id) => {
        let newEditItem = items.find((elem) => {
            return elem.id === id
        });
        console.log(newEditItem);

        setToggleSubmit(false);

        setInputData(newEditItem.name);

        setIsEditItem(id);
    }

    // delete Task
    const deleteItem = (index) => {
        // console.log(id);
        const updatedItems = items.filter((elem) => {
            return index !== elem.id;
        });

        setItems(updatedItems);
    }

    // remove all task
    const allClear = () => {
        setItems([]);
    }

    //add to local storage
    useEffect(() => {
        localStorage.setItem('lists', JSON.stringify(items))
    }, [items]);

    return (
        <main className="mt-4">
            <h1>Todo App</h1>
            <div className="card col-sm-4 mx-auto p-3">
                <div className="input-group ">
                    <input type="text" className="form-control" value={inputData} onChange={(e) => setInputData(e.target.value)} placeholder="Add Task" />
                    {
                        toggleSubmit ? <button className="btn btn-success ml-3" onClick={addItem}>Add Item</button> :
                            <button className="btn btn-primary ml-3" onClick={addItem}>Update Item</button>
                    }
                </div>

                {
                    items.map((elem) => {
                        return (
                            <div className="form-row mt-3" key={elem.id}>
                                <div className="col-md-8">
                                    <li className="list-group ">
                                        <p className="font-weight-light text-left" >{elem.name}</p>
                                    </li>
                                </div>
                                <div className="col-md-4">
                                    <button className="btn btn-warning " onClick={() => editItem(elem.id)}>Edit</button>
                                    <button className="btn btn-danger " onClick={() => deleteItem(elem.id)}>Delete</button>
                                </div>
                            </div>
                        )
                    })
                }
                <button className="btn btn-info mt-2" onClick={allClear}>All Clear</button>
            </div>
        </main>
    )
}
export default Todo;
